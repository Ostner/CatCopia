# CatCopia

A simple example which uses *Core Data* and a *custom collection view layout*.

Used API's:

* [the cat api](http://thecatapi.com/)
* [watson developer cloud](https://github.com/watson-developer-cloud/swift-sdk)

![demo](docs/images/demo.gif)
