//
//  ParserDelegate.swift
//  CatCopia
//
//  Created by Tobias Ostner on 3/15/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation

class ParserDelegate: NSObject, XMLParserDelegate {

    typealias Result = (id: String, url: String)

    private var elementName: String?
    private var urls: [String] = []
    private var ids: [String] = []
    private let completion: ([Result]) -> ()

    init(completion: @escaping ([Result]) -> ()) {
        self.completion = completion
        super.init()
    }

    func parser(_ parser: XMLParser,
                didStartElement elementName: String,
                namespaceURI: String?,
                qualifiedName qName: String?,
                attributes attributeDict: [String : String] = [:])
    {
        if elementName == "url" || elementName == "id" {
            self.elementName = elementName
        }
    }

    func parser(_ parser: XMLParser, foundCharacters string: String) {
        guard let elementName = elementName else { return }
        if elementName == "url" {
            urls.append(string)
        } else if elementName == "id" {
            ids.append(string)
        }
    }

    func parser(_ parser: XMLParser,
                didEndElement elementName: String,
                namespaceURI: String?,
                qualifiedName qName: String?)
    {
        self.elementName = nil
    }

    func parserDidEndDocument(_ parser: XMLParser) {
        completion(Array(zip(ids, urls)))
    }
}
