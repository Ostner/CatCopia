//
//  CatCell.swift
//  CatCopia
//
//  Created by Tobias Ostner on 3/15/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import VisualRecognitionV3

class CatCell: UICollectionViewCell {

    var imageURL: URL? {
        didSet {
            guard let imageURL = imageURL else { return }
            spinner.startAnimating()
            let group = DispatchGroup()
            loadImage(with: imageURL, group: group)
            loadClassification(with: imageURL, group: group)
            group.notify(queue: DispatchQueue.main) {
                self.spinner.stopAnimating()
                self.setContent(
                  image: self.image,
                  classification: self.classification)
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 10
        clipsToBounds = true
        layer.borderWidth = borderWidth
        layer.borderColor = UIColor.gray.cgColor
        contentView.backgroundColor = .darkGray

        imageView.backgroundColor = .clear
        contentView.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true

        contentView.addSubview(spinner)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        spinner.startAnimating()

        contentView.addSubview(titleLbl)
        titleLbl.translatesAutoresizingMaskIntoConstraints = false
        titleLbl.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: borderWidth).isActive = true
        titleLbl.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        titleLbl.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -borderWidth).isActive = true
        titleLbl.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.15).isActive = true
        titleLbl.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        titleLbl.textColor = .white
        titleLbl.text = ""
        titleLbl.isHidden = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        titleLbl.text = ""
        titleLbl.isHidden = true
    }

    override func preferredLayoutAttributesFitting(_ attrs: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        super.preferredLayoutAttributesFitting(attrs)
        let copy = attrs.copy() as! UICollectionViewLayoutAttributes
        copy.bounds = CGRect(x: 0, y: 0, width: 200, height: 200)
        return copy
    }

    // MARK: Private

    fileprivate let imageView = UIImageView()
    fileprivate let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    fileprivate let titleLbl = UILabel()
    fileprivate let borderWidth: CGFloat = 3
    fileprivate var image: UIImage?
    fileprivate var classification: String?

    fileprivate func loadImage(with url: URL, group: DispatchGroup) {
            group.enter()
            let resource = Resource(url: url) {
                data in
                return UIImage(data: data)
            }
            Webservice().load(resource: resource) {
                image in
                self.image = image
                group.leave()
            }
    }

    fileprivate func loadClassification(with url: URL, group: DispatchGroup) {
        group.enter()
        let recognition = VisualRecognition(apiKey: bluemixApiKey, version: "2017-03-21")
        let failure = { (error: Error) in
            group.leave()
            print(error) }
        recognition.classify(image: url.absoluteString, failure: failure) {
            classifiedImages in
            let classes = classifiedImages.images.first!.classifiers.first!.classes.sorted {
                $0.score > $1.score
            }
            self.classification = classes.first?.classification
            group.leave()
        }
    }

    fileprivate func setContent(image: UIImage?, classification: String?) {
        guard let image = image, let classification = classification else { return }
        imageView.image = image
        titleLbl.text = classification
        titleLbl.isHidden = false
    }

}
