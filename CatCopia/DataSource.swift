//
//  DataSource.swift
//  CatCopia
//
//  Created by Tobias Ostner on 3/13/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import CoreData

final class DataSource: NSObject, UICollectionViewDataSource, NSFetchedResultsControllerDelegate {

    fileprivate let collectionView: UICollectionView
    fileprivate let cellIdentifier: String
    fileprivate let fetchedResultsController: NSFetchedResultsController<Cat>
    fileprivate var inserted: [IndexPath] = []
    fileprivate var deleted: [IndexPath] = []

    init(collectionView: UICollectionView,
         cellIdentifier: String,
         fetchedResultsController: NSFetchedResultsController<Cat>)
    {
        self.collectionView = collectionView
        self.cellIdentifier = cellIdentifier
        self.fetchedResultsController = fetchedResultsController
        super.init()
        self.fetchedResultsController.delegate = self
        try! fetchedResultsController.performFetch()
        collectionView.reloadData()
    }

    // MARK: UICollectionViewDataSource

    func numberOfSections(in: UICollectionView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int)
      -> Int
    {
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath)
      -> UICollectionViewCell
    {
        print("cellForItemAt")
        let cell = collectionView.dequeueReusableCell(
          withReuseIdentifier: cellIdentifier,
          for: indexPath) as! CatCell
        let cat = fetchedResultsController.object(at: indexPath)
        cell.imageURL = URL(string: cat.source)!
        return cell
    }

    // MARK: NSFetchedResultsControllerDelegate

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        inserted = []
        deleted = []
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?)
    {
        switch type {
        case .insert:
            inserted.append(newIndexPath!)
        case .update:
            fatalError("not implemented yet")
        case .move:
            fatalError("not implemented yet")
        case .delete:
            deleted.append(indexPath!)
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView.performBatchUpdates(
          {
              self.collectionView.deleteItems(at: self.deleted)
              self.collectionView.insertItems(at: self.inserted)
          },
          completion: nil)
    }

}
