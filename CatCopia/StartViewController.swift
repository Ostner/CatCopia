//
//  ViewController.swift
//  CatCopia
//
//  Created by Tobias Ostner on 3/12/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import CoreData

class StartViewController: UICollectionViewController {

    var managedObjectContext: NSManagedObjectContext!

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = .white
        collectionView?.register(
          CatCell.self,
          forCellWithReuseIdentifier: cellIdentifier)
        let request = Cat.sortedFetchRequest
        let frc = NSFetchedResultsController(
          fetchRequest: request,
          managedObjectContext: managedObjectContext,
          sectionNameKeyPath: nil,
          cacheName: nil)
        dataSource = DataSource(
          collectionView: collectionView!,
          cellIdentifier: cellIdentifier,
          fetchedResultsController: frc)
        collectionView?.dataSource = dataSource
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshURLs), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        let deleteBtn = UIBarButtonItem(
          barButtonSystemItem: .trash,
          target: self,
          action: #selector(deleteCats))
        navigationItem.rightBarButtonItem = deleteBtn
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let collectionView = collectionView else { return }
        let layout = collectionView.collectionViewLayout as! CatCollectionFlowLayout
        let itemWidth = 0.75 * collectionView.bounds.width
        layout.minimumLineSpacing = -(itemWidth/2 * layout.standardScale)
        layout.estimatedItemSize = CGSize(
          width: itemWidth * layout.standardScale,
          height: itemWidth * layout.standardScale)
        layout.itemSize = CGSize(width: itemWidth, height: itemWidth)
        collectionView.contentInset = UIEdgeInsets(
          top: itemWidth/2,
          left: 0,
          bottom: itemWidth,
          right: 0)
    }

    // MARK: actions

    func refreshURLs() {
        let batchOfCats = Resource(url: catAPI) { return $0 }
        Webservice().load(resource: batchOfCats) {
            data in
            guard let data = data else { fatalError("No data from API call") }
            let parser = XMLParser(data: data)
            let parserDelegate = ParserDelegate() {
                [weak self] results in
                for result in results {
                    self?.managedObjectContext.performChanges {
                        _ = Cat.insert(
                          into: (self?.managedObjectContext)!,
                          id: result.id,
                          source: result.url)
                    }
                }
            }
            parser.delegate = parserDelegate
            parser.parse()
            self.collectionView?.refreshControl?.endRefreshing()
        }
    }

    func deleteCats() {
        let request = Cat.sortedFetchRequest
        let cats = try! managedObjectContext.fetch(request)
        managedObjectContext.perform {
            cats.forEach { self.managedObjectContext.delete($0) }
        }
    }

    // MARK: Private

    fileprivate let catAPI = URL(string: "http://thecatapi.com/api/images/get?api_key=\(catApiKey)&format=xml&results_per_page=10&type=jpg,png&size=full")!

    fileprivate let cellIdentifier = "CatCell"
    fileprivate var dataSource: DataSource! = nil


}
