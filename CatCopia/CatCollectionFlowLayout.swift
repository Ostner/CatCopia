//
//  CatCollectionFlowLayout.swift
//  CatCopia
//
//  Created by Tobias Ostner on 3/15/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class CatCollectionFlowLayout: UICollectionViewFlowLayout {

    let standardAlpha: CGFloat = 0.5
    let standardScale: CGFloat = 0.5

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let attributes = super.layoutAttributesForElements(in: rect) else { return nil }
        var result = [UICollectionViewLayoutAttributes]()
        for item in attributes {
            let copy = item.copy() as! UICollectionViewLayoutAttributes
            let ratio = calculateRatio(with: item.center.y)
            copy.alpha = ratio * standardAlpha + standardAlpha
            let scale = ratio * standardScale + standardScale
            copy.transform3D = CATransform3DScale(CATransform3DIdentity, scale, scale, 1)
            copy.zIndex = Int(copy.alpha * 10)
            result.append(copy)
        }
        return result
    }

    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }

    override func targetContentOffset(
        forProposedContentOffset proposedContentOffset: CGPoint,
        withScrollingVelocity velocity: CGPoint)
        -> CGPoint
    {
        guard let collectionView = collectionView,
              let attrs = layoutAttributesForElements(in: collectionView.bounds)
        else { return proposedContentOffset }
        let center = proposedContentOffset.y + collectionView.center.y
        let closest = attrs.sorted {
            abs($0.center.y - center) < abs($1.center.y - center)
        }.first ?? UICollectionViewLayoutAttributes()
        return CGPoint(
          x: proposedContentOffset.x,
          y: floor(closest.center.y - collectionView.center.y))
    }

    // MARK: Private

    private func calculateRatio(with center: CGFloat) -> CGFloat {
        let normalizedCenter = center - collectionView!.contentOffset.y
        let maxDistance = itemSize.height + minimumLineSpacing
        let distance = min(abs(collectionView!.center.y - normalizedCenter),
                           maxDistance)
        return (maxDistance - distance) / maxDistance
    }

}
