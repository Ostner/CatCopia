//
//  AppDelegate.swift
//  CatCopia
//
//  Created by Tobias Ostner on 3/12/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var persistentContainer: NSPersistentContainer!


    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?)
      -> Bool
    {
        window = UIWindow()
        let collectionVC = StartViewController(collectionViewLayout: CatCollectionFlowLayout())
        let navigationVC = UINavigationController(rootViewController: collectionVC)
        createCatCopiaContainer {
            container in
            self.persistentContainer = container
            collectionVC.managedObjectContext = container.viewContext
            self.window?.rootViewController = navigationVC
            self.window?.backgroundColor = .white
            self.window?.makeKeyAndVisible()
        }
        return true
    }

    private func createCatCopiaContainer(completion: @escaping (NSPersistentContainer) -> ()) {
        let container = NSPersistentContainer(name: "CatCopia")
        container.loadPersistentStores {
            _, error in
            guard error == nil else { fatalError("Failed to load stores: \(error)") }
            DispatchQueue.main.async { completion(container) }
        }
    }

}
