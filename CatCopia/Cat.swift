//
//  Cat.swift
//  CatCopia
//
//  Created by Tobias Ostner on 3/18/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import CoreData

final class Cat: NSManagedObject {

    @NSManaged fileprivate(set) var date: Date
    @NSManaged fileprivate(set) var imageId: String
    @NSManaged fileprivate(set) var source: String

    static func insert(into context: NSManagedObjectContext,
                       id: String,
                       source: String)
      -> Cat
    {
        let cat: Cat = context.insertObject()
        cat.imageId = id
        cat.source = source
        cat.date = Date()
        return cat
    }

}

extension Cat: Managed {
    static var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(key: "date", ascending: false)]
    }
}
